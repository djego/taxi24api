import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET) passengers', () => {
    return request(app.getHttpServer())
      .get('/passengers')
      .expect(200)
      .expect((res) => {
        expect(res.body).toBeInstanceOf(Array);
      });
  });
  it('/ (GET) drivers', () => {
    return request(app.getHttpServer())
      .get('/drivers')
      .expect(200)
      .expect((res) => {
        expect(res.body).toBeInstanceOf(Array);
      });
  });
  it('/ (GET) trips', () => {
    return request(app.getHttpServer())
      .get('/trips/active')
      .expect(200)
      .expect((res) => {
        expect(res.body).toBeInstanceOf(Array);
      });
  });

  describe('Finding drivers by location', () => {
    it('/ (GET) Finding 3 drivers near to location:', () => {
      return request(app.getHttpServer())
        .get('/drivers/near/-12.077723115951997/-77.05266128380238/3')
        .expect(200)
        .expect((res) => {
          expect(res.body).toBeInstanceOf(Array);
          expect(res.body.length).toEqual(3);
        });
    });
  });
});
