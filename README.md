# Taxi24  Backend Challenge 

## Tech Stack
- Typescript + NestJs + mongoose
- MongoDB cluster shared 

## Requirements 
- node 14.x or later
- npm 8.x or later 


## Install
First you need install with yarn
```bash
$ npm install 
```
Copy .env.example  to .env 
```bash
$ cp .env.example .env  
```
Then you insert string connection URI (Sent by email)
```bash
DATABASE_URI=mongodb+srv...
```

## Run 

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## API doc 

Go to [http://localhost:3000/api](http://localhost:3000/api)

## Test
For execute e2e tessting

```
$ npm run test:e2e
```

For execute testing coverage

```
$ npm run test:cov 
```

## TODO

* Add unit test
* Invoice module 
