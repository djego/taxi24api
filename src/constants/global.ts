export const MAX_DISTANCE_KM = 3;

export enum DriveStatus {
  AVAILABLE = 1,
  BUSY = 2,
}

export enum TripStatus {
  ACTIVE = 1,
  COMPLETE = 2,
}
