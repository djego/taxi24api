import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { DriverModule } from './api/driver/driver.module';
import { PassengerModule } from './api/passenger/passenger.module';
import { TripModule } from './api/trip/trip.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        uri: config.get<string>('DATABASE_URI'),
      }),
    }),
    DriverModule,
    PassengerModule,
    TripModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
