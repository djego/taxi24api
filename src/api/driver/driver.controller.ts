import { Body, Controller, Get, Param, Post } from '@nestjs/common';

import {
  Driver as IDriver,
  DriverById,
  DriverResponse as IDriverResponse,
  DriverByLocation,
} from './driver.interface';
import { DriverService } from './driver.service';

@Controller('drivers')
export class DriverController {
  constructor(private readonly driverService: DriverService) {}

  @Post()
  async addDriver(@Body() body: IDriver) {
    return await this.driverService.addDriver(body);
  }

  @Get()
  async getAllDrivers(): Promise<IDriverResponse[]> {
    return await this.driverService.getAllDrivers();
  }

  @Get('available')
  async getAvailableDrivers(): Promise<IDriverResponse[]> {
    return await this.driverService.getAvailableDrivers();
  }

  @Get(':id')
  async findOne(@Param() params: DriverById): Promise<IDriverResponse> {
    return await this.driverService.getDriver(params);
  }

  @Get('near/:lat/:long')
  async getByLocation(
    @Param() params: DriverByLocation,
  ): Promise<IDriverResponse[]> {
    return await this.driverService.getByLocation(params);
  }

  @Get('near/:lat/:long/:max')
  async getDriversByLocation(
    @Param() params: DriverByLocation,
  ): Promise<IDriverResponse[]> {
    return await this.driverService.getByLocation(params);
  }
}
