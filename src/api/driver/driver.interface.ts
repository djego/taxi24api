import { Types } from 'mongoose';

export interface DriverById {
  id: string;
}

export interface DriverByLocation {
  max?: number;
  lat: number;
  long: number;
}

export interface DriverResponse {
  _id: Types.ObjectId;
  name: string;
  lat: number;
  long: number;
}

export interface Driver {
  name: string;
  lat: number;
  long: number;
}
