import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  DriverById as IDriverById,
  DriverByLocation,
  Driver as IDriver,
  DriverResponse as IDriverResponse,
} from './driver.interface';
import {
  ADD_DRIVER_DTO_SCHEMA,
  GET_DRIVER_BY_LOCATION_DTO_SCHEMA,
} from './driver.validations';
import { Driver, DriverDocument } from './driver.schema';
import { Validation } from '../../shared/validations/validations';
import { Distance } from '../../shared/location/distance';
import { DriveStatus, MAX_DISTANCE_KM } from '../../constants/global';
@Injectable()
export class DriverService {
  constructor(
    private readonly validationUtils: Validation,
    private readonly distanceUtils: Distance,
    @InjectModel(Driver.name) private driverModel: Model<DriverDocument>,
  ) {}

  async addDriver(body: IDriver): Promise<IDriverResponse> {
    this.validationUtils.validationSchema(ADD_DRIVER_DTO_SCHEMA, body);

    const createdDriver = new this.driverModel({
      ...body,
      status: DriveStatus.AVAILABLE,
    });
    return createdDriver.save();
  }

  async getDriver(params: IDriverById): Promise<IDriverResponse> {
    this.validationUtils.validationId(params.id);

    return this.driverModel
      .findById(params.id)
      .select('name _id lat long')
      .exec();
  }

  async getAllDrivers(): Promise<IDriverResponse[]> {
    return this.driverModel.find().select('name _id lat long').exec();
  }

  async getAvailableDrivers(): Promise<IDriverResponse[]> {
    return this.driverModel
      .find({ status: DriveStatus.AVAILABLE })
      .select('name _id lat long status')
      .exec();
  }

  async getByLocation(params: DriverByLocation): Promise<IDriverResponse[]> {
    this.validationUtils.validationSchema(
      GET_DRIVER_BY_LOCATION_DTO_SCHEMA,
      params,
    );

    let drivers = await this.driverModel
      .find()
      .select('name _id lat long')
      .exec();
    const validDrivers = [];

    drivers.map((driver) => {
      const distance = this.distanceUtils.getDistance({
        driver: {
          lat: driver.lat,
          long: driver.long,
        },
        passenger: params,
      });

      if ('max' in params && params.max) {
        if (distance <= MAX_DISTANCE_KM && validDrivers.length < params?.max) {
          validDrivers.push(driver);
        }
      } else {
        if (distance <= MAX_DISTANCE_KM) {
          validDrivers.push(driver);
        }
      }
    });

    return validDrivers;
  }
}
