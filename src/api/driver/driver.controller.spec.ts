import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Distance } from '../../shared/location/distance';
import { Validation } from '../../shared/validations/validations';
import { DriverController } from './driver.controller';
import { Driver } from './driver.schema';
import { DriverService } from './driver.service';

describe('DriverController', () => {
  let controller: DriverController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DriverController],
      providers: [
        DriverService,
        { provide: getModelToken(Driver.name), useValue: jest.fn() },
        Validation,
        Distance,
      ],
    }).compile();

    controller = module.get<DriverController>(DriverController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
