import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Validation } from '../../shared/validations/validations';
import { PassengerController } from './passenger.controller';
import { Passenger } from './passenger.schema';
import { PassengerService } from './passenger.service';

describe('PassengerController', () => {
  let controller: PassengerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PassengerController],
      providers: [
        PassengerService,
        { provide: getModelToken(Passenger.name), useValue: jest.fn() },
        Validation,
      ],
    }).compile();

    controller = module.get<PassengerController>(PassengerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
