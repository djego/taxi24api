import { Types } from 'mongoose';

export interface Passenger {
  name: string;
}

export interface PassengerById {
  id: string;
}

export interface PassengerResponse {
  _id: Types.ObjectId;
  name: string;
}
