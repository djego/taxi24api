import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { Passenger, PassengerDocument } from './passenger.schema';
import {
  PassengerById,
  PassengerResponse,
  Passenger as IPassenger,
} from './passenger.interface';
import { ADD_PASSENGER_SCHEMA } from './passenger.validations';

import { Validation } from '../../shared/validations/validations';

@Injectable()
export class PassengerService {
  constructor(
    private readonly validationUtils: Validation,
    @InjectModel(Passenger.name)
    private passengerModel: Model<PassengerDocument>,
  ) {}

  async addPassenger(body: IPassenger): Promise<Passenger> {
    this.validationUtils.validationSchema(ADD_PASSENGER_SCHEMA, body);

    const createdPassenger = new this.passengerModel({ ...body, status: 1 });
    return createdPassenger.save();
  }

  async getPassenger(params: PassengerById): Promise<PassengerResponse> {
    this.validationUtils.validationId(params.id);

    return this.passengerModel.findById(params.id).select('name _id').exec();
  }

  async getAllPassenger(): Promise<PassengerResponse[]> {
    return this.passengerModel.find().select('name _id').exec();
  }
}
