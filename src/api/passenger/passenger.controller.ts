import { Body, Controller, Get, Param, Post } from '@nestjs/common';

import { PassengerService } from './passenger.service';
import {
  PassengerById,
  PassengerResponse as IPassengerResponse,
  Passenger as IPassenger,
} from './passenger.interface';

@Controller('passengers')
export class PassengerController {
  constructor(private readonly passengerService: PassengerService) {}

  @Post()
  async addPassenger(@Body() body: IPassenger) {
    return await this.passengerService.addPassenger(body);
  }

  @Get()
  async getAll(): Promise<IPassengerResponse[]> {
    return await this.passengerService.getAllPassenger();
  }

  @Get('/:id')
  async findOne(@Param() params: PassengerById): Promise<IPassengerResponse> {
    return await this.passengerService.getPassenger(params);
  }
}
