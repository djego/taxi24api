import { Types } from 'mongoose';

export interface Location {
  lat: number;
  long: number;
}

export interface Trip {
  date: Date;
  cost: number;
  driver: string;
  passenger: string;
  from: Location;
  to: Location;
}

export interface TripById {
  id: string;
}

export interface TripResponse {
  _id: Types.ObjectId;
  name: string;
  lat: number;
  long: number;
}
