import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';

import { TripService } from './trip.service';
import { TripById, Trip as ITrip } from './trip.interface';

@Controller('trips')
export class TripController {
  constructor(private readonly tripService: TripService) {}

  @Post()
  async createRequest(@Body() body: ITrip) {
    return await this.tripService.createRequest(body);
  }

  @Patch(':id')
  async complete(@Param() params: TripById) {
    return await this.tripService.completeTrip(params);
  }

  @Get('active')
  async getActiveTrips() {
    return await this.tripService.getActiveTrips();
  }
}
