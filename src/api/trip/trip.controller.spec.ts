import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Validation } from '../../shared/validations/validations';
import { TripController } from './trip.controller';
import { Trip } from './trip.schema';
import { TripService } from './trip.service';

describe('TripController', () => {
  let controller: TripController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TripController],
      providers: [
        TripService,
        { provide: getModelToken(Trip.name), useValue: jest.fn() },
        Validation,
      ],
    }).compile();

    controller = module.get<TripController>(TripController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
