import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Trip, TripDocument } from './trip.schema';
import { TripById, Trip as ITrip } from './trip.interface';
import { CREATE_REQUEST_SCHEMA } from './trip.validations';

import { Validation } from '../../shared/validations/validations';
import { TripStatus } from '../../constants/global';

@Injectable()
export class TripService {
  constructor(
    private readonly validationUtils: Validation,
    @InjectModel(Trip.name) private tripModel: Model<TripDocument>,
  ) {}

  async createRequest(body: ITrip): Promise<Trip> {
    this.validationUtils.validationSchema(CREATE_REQUEST_SCHEMA, body);

    const createdTrip = new this.tripModel({
      ...body,
      status: TripStatus.ACTIVE,
    });
    return createdTrip.save();
  }

  async completeTrip(params: TripById) {
    this.validationUtils.validationId(params.id);

    return this.tripModel
      .findOneAndUpdate(params, { status: TripStatus.COMPLETE })
      .select('id');
  }

  async getActiveTrips() {
    return this.tripModel.find({ status: 1 }).select('-__v').exec();
  }
}
